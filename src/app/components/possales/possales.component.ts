import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Pos } from 'src/app/store/model/pos.model';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/store/state';
import * as PosActions from '../../store/actions';

@Component({
  selector: 'app-possales',
  templateUrl: './possales.component.html',
  styleUrls: ['./possales.component.css']
})
export class PossalesComponent implements OnInit {
  pos: Observable<Pos[]>;
  id = 2;
  constructor(private store: Store<AppState>) { 
    this.pos = store.select('pos');
  }

  addPos(serial: string, a: number, q: number) {
    this.store.dispatch(new PosActions.AddPos({id: this.id++, serialNumber: serial, amount: a, quantity: q}));
  }

  deletePos(id) {
    this.store.dispatch(new PosActions.DeletePos(id));
  }

  ngOnInit() {
  }

}

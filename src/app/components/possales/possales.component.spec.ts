import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PossalesComponent } from './possales.component';

describe('PossalesComponent', () => {
  let component: PossalesComponent;
  let fixture: ComponentFixture<PossalesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PossalesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PossalesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

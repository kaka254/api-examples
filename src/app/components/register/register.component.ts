import { Component, OnInit } from '@angular/core';
import { FormBuilder , Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService} from '../../service/user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm = this.fb.group({
    email: ['', [Validators.required, Validators.email]],
    fullname: ['', [Validators.required, Validators.minLength(2)]],
    username: ['', [Validators.required, Validators.minLength(2)]],
    password: ['', [Validators.required, Validators.minLength(8)]]
  })
  constructor(private fb: FormBuilder,
              private route: Router,
              private userService: UserService) { }

  video = '../../../assets/video/video.mp4';
  ngOnInit() {
  }

  signUp() {
    this.userService.signUp(this.registerForm.value).subscribe(data => {
      console.log(data);
      this.route.navigate(['/login']);
    })
  }

  submit(): void {
    this.signUp();
    this.registerForm.reset();
  }

  loginForm() {
    this.route.navigate(['/login'])
  }

}

import { Component, OnInit} from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(private route: Router) { }

  ngOnInit() {
  }

  changeList() {
      this.route.navigate(['home/listpos']);
  }

  changeAdd() {
    this.route.navigate(['home/addpos']);
  }

  changeHome() {
    this.route.navigate(['home']);
  }

  logOut() {
    localStorage.setItem('Token', null);
    this.route.navigate(['/login'])
  }

  changeSale() {
    this.route.navigate(['home/listsale']);
  }
}


import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { TerminalService } from '../../service/terminal.service';

@Component({
  selector: 'app-addpos',
  templateUrl: './addpos.component.html',
  styleUrls: ['./addpos.component.css']
})
export class AddposComponent implements OnInit {

  addTerminal = this.fb.group({
    modelNumber: [''],
    issuedTo: [''],
    contact: ['']
});

  constructor(private fb: FormBuilder,
              private terminalService: TerminalService) { }

  ngOnInit() {
  }

  addTerminals() {
    this.terminalService.add(this.addTerminal.value).subscribe(
      data => console.log(data)
    );
  }

  submit(): void{
    this.addTerminals();
    this.addTerminal.reset();
  }

}

import { Component, OnInit, OnDestroy} from '@angular/core';
import { TerminalService } from '../../service/terminal.service';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-listpos',
  templateUrl: './listpos.component.html',
  styleUrls: ['./listpos.component.css']
})
export class ListposComponent implements OnInit, OnDestroy {
  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }
  terminal;
  dtOptions: DataTables.Settings = {}
  dtTrigger: Subject<any> = new Subject();
  constructor(private terminalService: TerminalService) { }

  ngOnInit() {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 2
    };
    this.listTerminals();
  }

  listTerminals() {
    this.terminalService.list().subscribe(
      data => {
        //console.log(data.terminals)
        this.terminal = data.terminals;
        this.dtTrigger.next();
      }
    );
  }

}

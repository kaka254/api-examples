import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListposComponent } from './listpos.component';

describe('ListposComponent', () => {
  let component: ListposComponent;
  let fixture: ComponentFixture<ListposComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListposComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListposComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

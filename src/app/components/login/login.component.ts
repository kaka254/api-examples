import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '../../service/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  video = '../../../assets/video/video.mp4';

  loginForm = this.fb.group({
    username: ['', [Validators.required, Validators.minLength(3)]],
    password: ['', [Validators.required, Validators.minLength(8)]]
});

  constructor(private fb: FormBuilder,
              private route: Router ,
              private userService: UserService) { }

  ngOnInit() {
  }

  logIn() {
    this.userService.logIn(this.loginForm.value).subscribe(data => {
      let token = data.headers.get('authorization');
      token = token + '';
      token = token.substring(7);
      if (token) {
        localStorage.setItem('Token', token);
        this.route.navigate(['/home']);
      }
    });
  }

  submit(): void {
    this.logIn();
    this.loginForm.reset();

  }

  registerForm() {
    this.route.navigate(['/register']);
  }

}

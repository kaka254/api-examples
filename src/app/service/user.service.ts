import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpHeaders , HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  url = 'http://localhost:8080/';

  constructor(public http: HttpClient) { }

  logIn(body): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };
    return this.http.post(`${this.url}login`, body, {observe: 'response'});
  }

  signUp(body): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };
    return this.http.post(`${this.url}user/sign-up`, body, httpOptions);
  }
}

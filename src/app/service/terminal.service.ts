import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TerminalService {
  url = 'http://localhost:8080/';
  token = localStorage.getItem('Token');
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': `Bearer ${this.token}`
    })
  };

  constructor(public http: HttpClient) { }

  add(data): Observable<any> {
    return this.http.post(`${this.url}terminal`, data, this.httpOptions);
  }

  list(): Observable<any> {
    return this.http.get(`${this.url}terminals`, this.httpOptions);
  }

  edit(id,body): Observable<any> {
    return this.http.put(this.url + 'terminal/' + id, body, this.httpOptions);
  }

  delete(id): Observable<any> {
    return this.http.delete(this.url + 'terminal' + id, this.httpOptions);
  }
}

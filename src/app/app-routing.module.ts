import { NgModule } from '@angular/core';
import { Routes, RouterModule, CanActivate } from '@angular/router';
import { AuthGuardService as AuthGuard} from './auth/auth-guard.service';

// import components
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent} from './components/register/register.component';
import { DashboardComponent} from './components/dashboard/dashboard.component';
import { ProfileComponent } from './components/profile/profile.component';
import { AddposComponent} from './components/addpos/addpos.component';
import { ListposComponent } from './components/listpos/listpos.component';
import { PossalesComponent} from './components/possales/possales.component';

const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'home', component: DashboardComponent, canActivate: [AuthGuard], children: [
    {path: '', redirectTo: 'profile', pathMatch: 'full' },
    { path: 'profile', component: ProfileComponent, canActivate: [AuthGuard] },
    { path: 'addpos', component: AddposComponent, canActivate: [AuthGuard] },
    { path: 'listpos', component: ListposComponent, canActivate: [AuthGuard] },
    { path: 'listsale', component: PossalesComponent, canActivate: [AuthGuard] }
  ]},
  {path: '', redirectTo: 'login', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

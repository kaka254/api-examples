import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule} from '@angular/forms';
import { JwtModule } from '@auth0/angular-jwt';
import { HttpClientModule } from '@angular/common/http';
import { StoreModule } from '@ngrx/store';
import { DataTablesModule } from 'angular-datatables';
import { actionReducer } from '../app/store/reducers';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { AddposComponent } from './components/addpos/addpos.component';
import { ListposComponent } from './components/listpos/listpos.component';
import { ProfileComponent } from './components/profile/profile.component';
import { BsDropdownModule } from 'ngx-bootstrap';
import { PossalesComponent } from './components/possales/possales.component';

export function tokenGetter() {
  return localStorage.getItem('Token');
}
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    DashboardComponent,
    AddposComponent,
    ListposComponent,
    ProfileComponent,
    PossalesComponent
  ],
  imports: [
    BrowserModule,
    DataTablesModule,
    StoreModule.forRoot({pos: actionReducer}),
    BsDropdownModule.forRoot(),
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        whitelistedDomains: ['http://localhost:4200']
      }
    }),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
